from fastapi import FastAPI
from books_route import router as books_router

app = FastAPI()
#memasukkan alamat routingnya books_route ke file utama
app.include_router(books_router) 

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applicatioins!"}


