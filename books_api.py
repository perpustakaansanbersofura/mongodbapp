from pymongo import MongoClient
from models.books_model import database as db
import csv 
import json
from bson import ObjectId

#api untuk dipanggil di main
db = db()

def objIdToStr(obj):
    return str(obj["_id"])

def search_books_by_name(**params):
    data_list = []
    for book in db.searchBookByName(**params):
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    print(data_list)
    return data_list

def search_books():
    data_list = []
    for book in db.showBooks():
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    return data_list

def search_books_id(**params):
    result = db.showBookById(**params)
    result["_id"] = objIdToStr(result)
    return result

def ubah_data (**params):
    try:
        db.updateBookById(params)
    except Exception as e:
        print(e)